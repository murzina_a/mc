from image_to_data import load_images, save_samples
from monte_carlo import sampling

from model import Model

DATA_PATH = "../mnist_png/training/"
MODEL_PATH = '../models/mnist_cnn.h5'


def do_sampling(objects):

    new_vectors_names = []
    for original_name, vector in objects:
        sampled_data = sampling(vector)
        new_vectors_names.extend(save_samples(original_name, sampled_data))

    return new_vectors_names


def get_class(img_addr):
    return int(list(img_addr)[0])


def test_model(new_samples):
    model = Model(MODEL_PATH)

    right = 0
    for data in new_samples:
        predicted = model.predict(data)
        actual = get_class(data)

        if predicted == actual:
            right += 1

    print("Actual accuracy is {}".format(right/len(new_samples)))

if __name__ == '__main__':
    vectors = load_images(DATA_PATH)
    new_samples_addrs = do_sampling(vectors)

    test_model(new_samples_addrs)
