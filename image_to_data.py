from scipy.ndimage import imread
from scipy.misc import imsave
from random import shuffle

import os


def matr2vec(matrix):
    vec = []
    for line in matrix:
        vec.extend(line)

    return vec


def vec2matr(vector):
    matrix = []

    size = 28
    vector = [vector[i:i + size] for i in range(0, len(vector), size)]

    for (vec) in vector:
        matrix.append(vec)

    return matrix


def load_images(path):
    dirs = os.listdir(path)

    files = []
    for dir in dirs:
        names = [path + dir + '/' + image for image in os.listdir(path + dir)]
        files.extend(names)

    shuffle(files)

    data = []
    for file in files[:1]:  # TODO
        data.append((file, matr2vec(imread(file))))

    return data


def get_image_name(image_path):
    return image_path.split('training/')[1]


def create_dir(original_image_name):
    dir_name = get_image_name(original_image_name).replace('.', '_')
    dir_name = dir_name.replace('/', '_')

    while True:
        try:
            os.mkdir(dir_name)
            return dir_name
        except FileExistsError:
            os.remove(dir_name)


def save_samples(original_image_name, samples):
    dir_name = create_dir(original_image_name)

    new_sample_names = []
    for n, sample in enumerate(samples):
        name = "{}/{}.png".format(dir_name, n)
        new_sample_names.append(name)

        imsave(name, vec2matr(sample))

    return new_sample_names
