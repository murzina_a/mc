import random
import numpy as np

MU = 0
SIGMA = 0.1


def sampling(data_orig):
    size = len(data_orig)

    new_data = []
    for _ in range(5):
        n = 1
        for _ in range(5):
            data = data_orig.copy()
            level = random.sample([0.01, 0.1, 0.2, 0.3], 1)[0]
            n_change = int(size * level)
            positions = random.sample(range(0, size), n_change)

            for x in positions:
                data[x] = abs(data[x] + int(np.ceil(np.random.normal(MU, SIGMA, 1)[0] * n)))

            new_data.append(data)

            n *= 10

    return new_data
