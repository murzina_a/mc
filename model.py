from keras.models import load_model
from keras.optimizers import Adam
from scipy.ndimage import imread

import numpy as np


class Model(object):
    def __init__(self, path):
        self.model = load_model(path)
        self.model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])

    def predict(self, img_path):
        img = imread(img_path)
        img = np.reshape(img, [1, 28, 28, 1])
        classes = self.model.predict_classes(img)

        return int(classes[0])
